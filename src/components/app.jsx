import React, { Component } from 'react';
import { withRouter } from "react-router";

class App extends Component {
  onNextButtonClick = () => {
    this.props.history.push('/other');
  }

  render() {
    return (
      <div className="App">
        <p>This is the main page.</p>
        <button onClick={() => this.onNextButtonClick()}>
          Goto next page
        </button>
      </div>
    );
  }
}

export default withRouter(App);
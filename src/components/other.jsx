import React from 'react';
import { withRouter } from 'react-router';

class Other extends React.Component {
  onBackButtonClick = () => {
    this.props.history.goBack();
  }

  render = () => (
    <div>
      <p>This is the other page.</p>
      <button onClick={() => this.onBackButtonClick()}>
        Go back
      </button>
    </div>
  );
}

export default withRouter(Other);
import React from 'react';
import { Route, BrowserRouter } from "react-router-dom";

import App  from "../components/app";
import Other from '../components/other'

export default class Root extends React.Component {
  render = () => (
    <BrowserRouter>
      <Route
        exact
        path='/'
        component={App}
      />
      <Route
        exact
        path='/other'
        component={Other}
      />
    </BrowserRouter>
  );
}
